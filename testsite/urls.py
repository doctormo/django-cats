from django.conf.urls import patterns, include, url

from cats import cat_in_tree
from .views import TestView

from django.contrib import admin

urlpatterns = patterns('',
    url(r'^admin/',     include(admin.site.urls)),

    cat_in_tree(r'', TestView.as_view(), 'foreign', 'manyto'),
)

