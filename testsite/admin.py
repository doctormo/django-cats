#
# This admin used to construct fixtures for tests
#
from django.contrib.admin import *
from .models import CategoryA, CategoryB, CategoryC, ListItem

site.register(CategoryA)
site.register(CategoryB)
site.register(CategoryC)
site.register(ListItem)


