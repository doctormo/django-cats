"""
Test models for testing django-cats
"""

from django.db.models import *

class CategoryA(Model):
    name = CharField(max_length=32)

    def __str__(self):
        return self.name

class CategoryB(Model):
    name = CharField(max_length=32)

    def __str__(self):
        return self.name

class CategoryC(Model):
    name = CharField(max_length=32)
    items = ManyToManyField('ListItem', related_name='manyfrom', blank=True)

    def __str__(self):
        return self.name

CategoryD = (
  ('d1', 'Delta Dog'),
  ('d2', 'Delta Cat'),
  ('d3', 'Delta Parrot'),
  ('d4', 'Delta Fish'),
  ('d5', 'Delta Elephant'),
  ('d6', 'Delta Snake'),
)

class ListItem(Model):
    name = CharField(max_length=32)

    # Test a direct relationship choice
    foreign = ForeignKey(CategoryA, null=True, blank=True)

    # Test a many to many relationship
    manyto = ManyToManyField(CategoryB, blank=True)

    # Test reverse many to many relationship
    # manyfrom = (see CategoryC class)

    # Test fixed set of choices
    choice = CharField(max_length=16, choices=CategoryD, null=True, blank=True)

    # Test arbitary set of possibilities
    arbitary = CharField(max_length=16, null=True, blank=True)

    def __str__(self):
        return self.name



