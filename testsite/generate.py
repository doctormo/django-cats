#
# Generates data for fixtures (only run when needed)
#
from testsite.models import *
from random import choice, sample, randint

ITEMS = 64
SAMPLE = 5
CHOICES = ['Dog','Cat','Parrot','Fish','Elephant','Snake']

OBJS = [[None], [], [], [(None, "Delete Me")], [None, '']]

for x in range(len(CHOICES)):
    OBJS[0].append(CategoryA.objects.create(name='Alpha %s' % CHOICES[x]))
    OBJS[1].append(CategoryB.objects.create(name='Beta %s' % CHOICES[x]))
    OBJS[2].append(CategoryC.objects.create(name='Chart %s' % CHOICES[x]))
    OBJS[3].append( ('d%d' % (x+1), "Delta %s" % CHOICES[x]) )
    OBJS[4].append("Echo %s" % CHOICES[x])

print "Set these choices into the code:\nCategoryD = (\n  " + \
      ",\n  ".join(str(i) for i in OBJS[3]) + ",\n)"

for x in range(ITEMS):
    obj = ListItem.objects.create(name='Item #%d' % (x+1),
        foreign=choice(OBJS[0]),
        choice=choice(OBJS[3])[0],
        arbitary=choice(OBJS[4]),
    )
    
    for (key, objs) in [('manyto', OBJS[1]), ('manyfrom', OBJS[2])]:
        for a in range(randint(0, SAMPLE)):
            getattr(obj, key).add(choice(objs))

