"""
Test Views
"""

from cats import CategoryListView, CategoryFilter
from .models import ListItem

class TestView(CategoryListView):
    foreign = CategoryFilter("Category Ayes", )

    class Meta:
        model = ListItem

