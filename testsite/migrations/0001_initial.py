# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='CategoryA',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=32)),
            ],
        ),
        migrations.CreateModel(
            name='CategoryB',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=32)),
            ],
        ),
        migrations.CreateModel(
            name='CategoryC',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=32)),
            ],
        ),
        migrations.CreateModel(
            name='ListItem',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=32)),
                ('choice', models.CharField(blank=True, max_length=16, null=True, choices=[(b'c1', b'Choice A'), (b'c2', b'Choice B'), (b'c3', b'Choice C')])),
                ('arbitary', models.CharField(max_length=16, null=True, blank=True)),
                ('foreign', models.ForeignKey(blank=True, to='testsite.CategoryA', null=True)),
                ('manyto', models.ManyToManyField(to='testsite.CategoryB', blank=True)),
            ],
        ),
        migrations.AddField(
            model_name='categoryc',
            name='items',
            field=models.ManyToManyField(related_name='manyfrom', to='testsite.ListItem', blank=True),
        ),
    ]
