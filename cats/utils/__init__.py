#
# Copyright (C) 2015, Martin Owens <doctormo@gmail.com>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
"""
Basic utilities for use with django-cats
"""

def get_objects(model, lookup, qs=None):
    """
    Returns a list of models based on a values_list lookup, allowing
    complex queries to return lists of real objects.
    """
    if qs is None:
        qs = model.objects.all()
    else:
        # Clear any existing ordering
        qs.order_by()

    target = model
    for field in lookup.split('__'):
        target = target._meta.get_field(field).rel.to

    pks = qs.distinct().values_list(lookup, flat=True)
    return target.objects.filter(pk__in=pks)


def reverse_order(o, active=True):
    """Add or remove a minus sign to an sort order column"""
    if not active:
        return o
    if o[0] == '-':
        return o[1:]
    return '-' + o


def delay_call(method, *args, **kwargs):
    """Save a method call with args and kwargs for later"""
    def internal():
        return method(*args, **kwargs)
    return internal


def clean_dict(a, tr=None):
    """Removes any keys where the values is None"""
    for key in a.keys():
        if a[key] is None:
            a.pop(key)
        elif tr:
            a[key] = tr.get(str(a[key]), a[key])
    return a


import types

def cached(f):
    """We save the details per request"""
    key = '_' + f.__name__
    def _call(self, *args, **kwargs):
        target = self.request
        field = key
        if args:
            field += '_' + ('_'.join(a for a in args if isinstance(a, basestring)))
        if not hasattr(target, field):
            ret = f(self, *args, **kwargs)
            if isinstance(ret, types.GeneratorType):
                ret = list(ret)
            setattr(target, field, ret)
        return getattr(target, field)
    return _call

