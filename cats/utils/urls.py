#
# Copyright (C) 2014, Martin Owens <doctormo@gmail.com>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
"""
Provides non-essential utilities for url definitions with categories.
"""

__all__ = ('url_tree', 'cat_in_tree',)

from django.conf.urls import patterns, url, include

def url_tree(regex, *urls):
    """
    Provides a simple way to contruct url trees, where the base url is the same. 

      url_tree(r'^blog/(?<pk>\d)/',
        url(r'^$',     View.as_view()),
        url(r'^edit/', Edit.ad_view()),
      ),
    """
    return url(regex, include(patterns('', *urls)))


def cat_in_tree(regex, view, this, *names, **kwargs):
    """Contructs a url tree list of urls based on available categories
       in requested names:

       cat_in_tree('^base/', view.as_view(), 'cat1', 'cat2', name='foo')

       Each category specified will be added to the url and every url will
       have the same url_name for category resolution.
    """
    #
    # This needs to be better, allowing for prefixes and so on. [all|\d|\s] for example XXX
    #
    # Detect class based view that is not initialised and call as_view() XXX
    #
    # XXX the exact regex required depends a lot on the filter named on the view.
    #     we should be able to use this information to set the right slug field
    #     name as well as the right show. We might want a as_url() class method
    #     for the view to clear the API further than it has even been cleared.
    #
    next_url = cat_in_tree
    next_regex = r'^(?P<%s>[^\/]*)/' % this
    if len(names) == 0:
        next_url = url
        next_regex += '$'

    return url_tree(regex,
      url(r'^$', view, **kwargs),
      next_url(next_regex, view, *names, **kwargs),
    )

