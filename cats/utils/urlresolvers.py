#
# Copyright (C) 2015, Martin Owens <doctormo@gmail.com>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
"""
Utilities used by Category View to formulate urls where the categories
might be defined by the url and not by the querystring.
"""

from django.core.urlresolvers import reverse, get_resolver
from django.utils.http import urlquote

def get_possible_args(view_name):
    """Returns a generator with all possible kwargs for this view name"""
    resolver = get_resolver(None)
    possibilities = resolver.reverse_dict.getlist(view_name)
    for possibility, pattern, defaults in possibilities:
        for r, params in possibility:
            for p in params:
                # Remove non-keyword arguments
                if p[0] != '_':
                    yield p

def get_url(url_name, url_qs, url_args, **kwargs):
    """Formulates a url based on the view and it's existing filters

      url_name - The name of the url as found in urls.py
      url_qs   - Dictionary of values passed in via GET (querystring)
      url_args - Dictionary of values passed in via URL (url regex)

      key=value - Replacement value in the returned url;
                  if None, key is removed from resulting url.

    """
    qs = url_qs.copy()
    ua = url_args.copy()
    args = list(get_possible_args(url_name))

    for (key, value) in kwargs.items():
        target = ua if key in args else qs
        if value is None:
            target.pop(key, None)
        else:
            target[key] = value

    url = reverse(view_url, kwargs=kwargs)

    # Always remove page, start from begining
    query = ('&'.join('%s=%s' % (a, urlquote(b)) for (a, b) in qs.items() \
                                   if a not in ['page'] and b is not None))
    if query:
        url += '?' + query
    return url

