#
# Copyright (C) 2015, Martin Owens <doctormo@gmail.com>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
"""
Category filtering django View class. (see README for details)
"""

__all__ = ('CategoryListView',)

from django.views.generic.base import TemplateView
from django.db.models import Model

from .utils import cached, clean_dict, reverse_order
from .utils.urlresolvers import get_url
#from .search_base import get_haystack_query, MR_OD, RMR_OD
from .filters import CategoryBase

class CategoryListView(TemplateView):
    """ListView with categorisation functionality, provides a simple way to
    Define a set of categories and have them availab ein the template with urls"""
    order    = None
    rss_view = None
    sorting  = None
    redirect = False
    using_db = 'default'

    @property
    def model(self):
        raise NotImplementedError("Model must exist in CategoryListView")

    def __init__(self):
        super(CategoryListView, cls).__init__()

        # We want to make sure all categories understand their
        # own context with this View class and how they were created
        self.categories = []
        for (name, value) in self.__dict__.items():
            if isinstance(value, CategoryBase):
                self.categories.append(value.provoke(self, name))

    def base_queryset(self):
        return self.Meta.model._default_manager.all()

    def get_queryset(self, **kwargs):
        self.query = self.get_value('q', None)
        def _get(cat):
            field = getattr(self.model, cat.cid, None)
            if field and (not isinstance(field, (MR_OD, RMR_OD)) or not self.query):
                return cat.item
            return cat.item.value

        if self.query:
            queryset = get_haystack_query(self.query, using=self.using_db,
                models=(self.model,))
        else:
            queryset = self.base_queryset()
        filters = dict((cat.cid, _get(cat)) for cat in self.get_cats() if cat)

        filters.update(clean_dict(dict(self.get_opts())))
        filters.update(kwargs)
        qs = queryset.filter(**clean_dict(filters, {'True':True,'False':False}))
        ob = self.get_value('order', self.order)
        if ob:
            try:
                return qs.order_by(ob)
            except:
                pass
        return qs

    def get_url(self, view=None, **kwargs):
        """Returns a url with this key=value replaced for this url"""
        view = view or self.request.resolver_match.url_name
        return get_url(view, self.kwargs, self.request.GET, **kwargs)

    def get_count(self, cid, item=None, value=None):
        """Attempts to execute the query with the proposed filter
           to find the prospective count"""
        if not hasattr(self.model, cid):
            item = item.value
        if item is None or getattr(item, 'value', item) is None:
            item = None
        return self.get_queryset(**{cid: item}).count()

    def get_value(self, key, default=None):
        return self.kwargs.get(key, self.request.GET.get(key, default))

    @cached
    def get_opts(self):
        return [self.get_opt(*opt) for opt in self._opts()]

    @classmethod
    def _opts(cls):
        """Returns a set of filtered links for manually defined options"""
        for (cid, link) in cls.opts:
            field = cid
            if '__' in link:
                (nfield, rest) = link.split('__', 1)
                if not hasattr(cls.model, field)\
                   and hasattr(cls.model, nfield):
                     (field, link) = (nfield, rest)
            yield (cid, link, field)
        # Yield extra options here via automatic association?

    def get_opt(self, cid, link, field, context=False):
        """Returns a value suitable for filtering"""
        value = self.get_value(cid)
        if self.query and not context:
            # No object lookup for haystack search
            return (cid, value)

        if value is not None:
            if hasattr(self.model, field):
                mfield = getattr(self.model, field)
                try:
                    values = mfield.get_queryset().filter(**{link: value})
                    if values.count() == 1:
                        value = values[0]
                    else:
                        value = [ v for v in values ]
                        field = field + '__in'
                except mfield.field.rel.to.DoesNotExist:
                    value = None
            elif link.split('__')[-1] in ('isnull',):
                field = link
            elif link and not context:
                raise ValueError("Can't find the model for field: %s.%s" % (self.model.__name__, link))
        return (context and cid or field, value)

    @cached
    def get_value_opts(self):
        """Similar to get_opt, but returns value useful for templates"""
        return [self.get_opt(*opt, context=True) for opt in self._opts() ]

    @cached
    def get_cats(self):
        return [ self.get_cat(*cat) for cat in self.cats ]

    def get_cat(self, cid, name, model=None):
        # We could move this into Category class XXX
        field = getattr(self.model, cid, None)
        if field and hasattr(field, 'get_queryset'):
            qs = field.get_queryset()
        elif model:
            if isinstance(model, Model):
                qs = model.objects.all()
            elif isinstance(model, basestring):
                qs = getattr(self, model)()
        else:
            raise KeyError(("The field '%s' isn't a ForeignKey, please add "
                           "the linked model for this category.") % cid)
        if qs is None:
            return None
        return Category(self, qs, cid, name)

    def get(self, *args, **kwargs):
        qs = self.get_queryset()
        if self.redirect and not self.query and qs.count() == 1:
            item = qs.get()
            if hasattr(item, 'get_absolute_url'):
                return redirect( item.get_absolute_url() )
        context = self.get_context_data(object_list=qs)
        return self.render_to_response(context)

    def get_template_names(self):
        opts = self.model._meta
        return ["%s/%s_list.html" % (opts.app_label, opts.object_name.lower())]
 
    def get_context_data(self, **data):
        # this allows search results and object queries to work the same way.
        if not hasattr(self.model, 'object'):
            self.model.object = lambda self: self

        data['query'] = self.get_value('q')
        data['orders'] = self.get_orders()
        data['categories'] = self.get_cats()
        data.update(((cat.cid, cat.item) for cat in self.get_cats() if cat))
        data.update(self.get_value_opts())
        
        if self.rss_view:
            data['rss_url'] = self.get_url(view=self.rss_view)
        data['clear_url'] = self.get_url(q=None)
        return data

    def get_orders(self):
        order = self.get_value('order', self.order)
        for (o, label) in self.orders:
            yield { 'id': o, 'name': label, 'down': '-' == (order or '*')[0],
                'active': order.strip('-') == o.strip('-'),
                'url': self.get_url(order=reverse_order(o, o == order)) }

