"""
A django ListView replacement with advanced category and search index filtering
"""
__pkgname__ = 'django-cats'
__version__ = '0.2'

from .utils.urls import *
from .views import *
from .filters import *
from .feed import *

