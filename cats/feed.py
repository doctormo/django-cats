#
# Copyright (C) 2015, Martin Owens <doctormo@gmail.com>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
"""
Provide an RSS Feed for any possible filterable list view.
"""

__all__ = ('CategoryFeed',)

from django.contrib.syndication.views import Feed

class CategoryFeed(Feed):
    """So these CategoryListViews can become Feeds we have to do a few bits for django"""
    def __call__(self, request, *args, **kwargs):
        self.request = request
        self.kwargs = kwargs
        return Feed.__call__(self, request, *args, **kwargs)

    def items(self):
        return self.get_queryset()

    def link(self):
        return self.get_url()

    # XXX it might be possible to generate a title and description here.

