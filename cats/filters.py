#
# Copyright (C) 2015, Martin Owens <doctormo@gmail.com>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
"""
CategoryFilter for adding a selectable category to a CategoryListView.

 XXX more documentation here XXX

"""

__all__ = ('AllCategory', 'CategoryFilter', )

from inspect import getargspec, getmro

class AllCategory(object):
    """A simple object for 'All' Menu Item"""
    value = None

    @property
    def name(self):
        return _("All")

    def __str__(self):
        return "All"

class CategoryBase(object):
    """
    Specific documentation about properties here...
    """
    def __init__(self, **kwargs):
        self.view = None
        self.category_id = None
        self._selected = None
        self._items = None
        self._kwargs = kwargs

        for (name, value) in kwargs.items():
            if hasattr(self, name) and getattr(self, name) is not None:
                if value is None:
                    continue
                raise AttributeError("Attribute %s already set" % name)
            setattr(self, name, value)

    def provoke(self, view, category_id):
        """Creates a copy of itself with the new initialised view object"""
        if self.view is not None:
            raise RuntimeError("Refusing to provoke Category a second time.")
        return type(self)(**self._kwargs)._inner_provoke(view, category_id)

    def _inner_provoke(self, view, category_id)
        """Initialise and create this category object"""
        self.category_id = category_id
        self.view = view
        self.value = view.get_value(category_id)
        return self

    def get_items(self):
        if self.category_id is None:
            raise RuntimeError("Category must be provoked before use.")
        if self._items is None:
            self._items = list(self.populate_items())
        return self._items

    @property
    def selected(self):
        if self._selected is None:
            for item in self.get_items():
                if item == self.value:
                    self._selected = item
        return self._selected

    def active_items(self):
        return [ item for item in self if item.count() ]

    def count(self):
        """Returns the number of active items"""
        return len(self.active_items()) - 1

    def __str__(self):
        return getattr(self, 'label', self.category_id)

    def __eq__(self, value):
        return self.selected == value

    def __nonzero__(self):
        return self.selected != self[0]


class ForienKeyCategory(CategoryBase):
    """A category created by a foreign key reference."""
    def populate_items():
        self.item = AllCategory()
        # Populate items, mostly these models don't have slug columns.
        self.append( self.item )
        for item in queryset:
            if self.value is not None and item.value == self.value:
                self.item = item
            self.append(item)
        for item in self:
            item.url = delay_call(view.get_url, cid, item.value)
            item.count = delay_call(view.get_count, cid, item)

