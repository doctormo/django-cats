#
# Copyright (C) 2015, Martin Owens <doctormo@gmail.com>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
"""
Unit test core functionality without any view calls (C testsite for full tests)
"""

from django.test import TestCase, Client
from django.conf import settings
from cats import *

class SimpleCase(TestCase):
    fixtures = []

    def test_00_import(self):
        l = globals()
        self.assertIn('CategoryListView', l)
        self.assertIn('CategoryFilter', l)
        self.assertIn('CategoryFeed', l)
        self.assertIn('cat_in_tree', l)

    def test_01_tree(self):
        """Test tree url creation"""
        ret = cat_in_tree('^url/', 'my_view', 'catA', 'catB')

        for name in ['^(?P<catA>[^\/]*)/', '^(?P<catB>[^\/]*)/$']:
            self.assertEqual(type(ret).__name__, 'RegexURLResolver')
            urls = ret._urlconf_module
            self.assertEqual(len(urls), 2)
            self.assertEqual(type(urls[0]).__name__, 'RegexURLPattern')
            self.assertEqual(urls[0]._regex, r'^$')
            self.assertEqual(urls[1]._regex, name)
            ret = urls[1]

        self.assertEqual(type(ret).__name__, 'RegexURLPattern')

    def test_10_view(self):
        """Test creating a CategoryListView"""
        class TestView(CategoryListView):
            pass

        view = TestView.as_view()

